﻿using UnityEngine;
using System.Collections;

public class BlueToken4 : MonoBehaviour {

    public static BlueToken4 Instance;
    int blue4Index;
    static Animator animator;
    // Use this for initialization
    public Animation anim;
    void Start()
    {
        animator = GetComponent<Animator>();
        blue4Index = 39;
        ////
        /* anim = GetComponent<Animation>();
         AnimationCurve curve = AnimationCurve.Linear(0, 1, 2, 3);
         AnimationClip clip = new AnimationClip();
         clip.legacy = true;
         clip.SetCurve("", typeof(Transform), "localPosition.x", curve);
         anim.AddClip(clip, "test");
         anim.Play("test");*/
        Instance = this;
    }

    // Update is called once per frame

    public void player4Start()
    {
        //if(Input.GetMouseButtonDown(0))

        {
            print("Player 1 Turn");
            //Condition for first start by 6
            if (DiceSwipeControl.Instance.diceCount == 6)
            {
                TokenMove.Instance.board[39] = (int)TokenNo.tokens.blueToken;
                animator.SetInteger("AnimState", 1);


            }
            else
            {
                animator.SetInteger("AnimState", 0);
                //Dice.playerTurn++;
            }

        }

    }
    public void player4Turn()
    {

        if (TokenMove.Instance.board[blue4Index] == 4)
        {
            blue4Index = +DiceSwipeControl.Instance.diceCount;
            TokenMove.Instance.board[blue4Index] = (int)TokenNo.tokens.blueToken;
            // for (int i = 1; i <= DiceSwipeControl.Instance.diceCount; i++)
            {
                //animator.Play("YellowFirstMove1-2");
                animator.SetInteger("TokenMove", DiceSwipeControl.Instance.diceCount);

            }
        }
        if (blue4Index > 52)
        {
            blue4Index = 0;
        }
    }
}
