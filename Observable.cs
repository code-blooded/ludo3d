/*using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Collections;

namespace LUDO_DesignPatterns
{
    public class LudoModel : Observable
    {
        private static LudoModel uniqueInstance;
        private Die die;
        private Board board;
        private bool lastRollWasSix = false;
        private List<Player> players;
        private List<Observer> Observers= new List<Observer>();
        private int playersturn;
        public Player playersTurn = null;
        private Player playerWon = null;
        public Action nextAction=Action.Nothing;

        

        public Die Die
        {
            
            get { return this.die; }
        }

        public Board Board
        {
            get { return this.board; }
        }

        public List<Player> Players
        {
            get { return this.players; }
        }



      public void RegisterObserver(Observer o)
        {
            Observers.Add(o);

        }
        
        public void NotifyObservers()
        {
            foreach (Observer o in Observers)
            {
                o.UpdateView();

            }
    
        }

     

        

        private LudoModel()
        {
            
            
                
            
          
        }

        public static LudoModel getInstance() 
        {
            
            lock (typeof(LudoModel))
            {

                if (uniqueInstance == null)
                {
                    uniqueInstance = new LudoModel();
                }
            }
		
		return uniqueInstance;
	}


        public void StartGame(List<Player> Players)
        {

            
            this.players = Players;
            playersTurn = this.players[0];
            playersturn = 0;
            die=new Die();
            this.board = new Board(players);
            nextAction = Action.RollDie;
            lastRollWasSix = false;

            NotifyObservers();

        }

        public void EndGame()
        {
            lastRollWasSix = false;
            nextAction = Action.EndGame;
            NotifyObservers();

        }

     
        public bool areAllFinished()
        {
            int playerswoncount = 0;
            foreach (Player player in this.players)
            {
                if (player.HasWon)
                {
                    playerswoncount++;
                }
            }

            if (this.players.Count - 1 == playerswoncount)
            {
                return true;
            }

            else
            {

                return false;
            }
        }

        public void MovePiece(Color PieceColor,int CurrentPosition)
        {
            if (nextAction == Action.MovePiece && playersTurn.Color==PieceColor)
            {
                if (this.board.Squares[CurrentPosition].Pieces.Count > 0)
                {
                    if (this.board.Squares[CurrentPosition].Pieces[0].Color == PieceColor)
                    {
                        if (canMovePiece(playersTurn, Die))
                        {
                            int targetindex = CalculateTargetSquareIndex(PieceColor, CurrentPosition);

                            if (targetindex != -1)
                            {
                                if (this.board.Squares[targetindex].Pieces.Count > 0)
                                {
                                    if (this.board.Squares[targetindex].Pieces[0].Color != PieceColor)
                                    {
                                        int index = homeindex(this.board.Squares[targetindex].Pieces[0].Color);


                                        
                                        




                                        this.board.MovePiece(this.board.Squares[targetindex].Pieces[0], this.board.Squares[index]);
                                        this.board.MovePiece(this.board.Squares[CurrentPosition].Pieces[0], this.board.Squares[targetindex]);

                                        if (!lastRollWasSix)
                                        {
                                            NextPlayersTurn();

                                        }

                                        nextAction = Action.RollDie;

                                        foreach (Player player in this.Players)
                                        {
                                            UpdatePlayer(player);
                                        }
                                    }

                                    else
                                    {
                                        this.board.MovePiece(this.board.Squares[CurrentPosition].Pieces[0], this.board.Squares[targetindex]);


                                        if (!lastRollWasSix)
                                        {
                                            NextPlayersTurn();

                                        }

                                        nextAction = Action.RollDie;
                                        foreach (Player player in this.Players)
                                        {
                                            UpdatePlayer(player);
                                        }

                                    }


                                }

                                else
                                {
                                    this.board.MovePiece(this.board.Squares[CurrentPosition].Pieces[0], this.board.Squares[targetindex]);

                                    if (!lastRollWasSix)
                                    {
                                        NextPlayersTurn();

                                    }

                                    nextAction = Action.RollDie;
                                    foreach (Player player in this.Players)
                                    {
                                        UpdatePlayer(player);
                                    }

                                }

                            }
                            

                        }

                    }



                }

            }

            if (areAllFinished())
            {
                nextAction = Action.Nothing;
            }

            NotifyObservers();
        
        }

        public void UpdatePlayer(Player Player)
        {
            int goatsout = 0;
            if (Player.Color == Color.Red)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[88 + i].Pieces.Count == 0)
                    {
                        goatsout++;

                    }


                }

                Player.GoatsOut = goatsout;

                if (this.board.Squares[75].Pieces.Count == 4)
                {
                    Player.HasWon = true;
                }
                else
                {
                    Player.no_of_goatswon = this.board.Squares[75].Pieces.Count;
                }
            }

            else if (Player.Color == Color.Yellow)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[76 + i].Pieces.Count == 0)
                    {
                        goatsout++;

                    }


                }

                Player.GoatsOut = goatsout;

                if (this.board.Squares[57].Pieces.Count == 4)
                {
                    Player.HasWon = true;
                }
                else
                {
                    Player.no_of_goatswon = this.board.Squares[57].Pieces.Count;
                }
            }

            else if (Player.Color == Color.Blue)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[84 + i].Pieces.Count == 0)
                    {
                        goatsout++;

                    }


                }

                Player.GoatsOut = goatsout;

                if (this.board.Squares[69].Pieces.Count == 4)
                {
                    Player.HasWon = true;
                }
                else
                {
                    Player.no_of_goatswon = this.board.Squares[69].Pieces.Count;
                }

            }

            else if (Player.Color == Color.Green)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[80 + i].Pieces.Count == 0)
                    {
                        goatsout++;

                    }


                }

                Player.GoatsOut = goatsout;

                if (this.board.Squares[63].Pieces.Count == 4)
                {
                    Player.HasWon = true;
                }
                else
                {
                    Player.no_of_goatswon = this.board.Squares[63].Pieces.Count;
                }
            }



           

            

        }
        public int homeindex(Color PieceColor)
        {
            if (PieceColor == Color.Red)
            {
                for(int i=0;i<4;i++)
                {
                    if(this.board.Squares[88+i].Pieces.Count==0)
                    {
                        return i+88;

                    }

                }

            }

            else if (PieceColor == Color.Yellow)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[76 + i].Pieces.Count == 0)
                    {
                        return i + 76;

                    }

                }

            }

            else if (PieceColor == Color.Blue)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[84 + i].Pieces.Count == 0)
                    {
                        return i + 84;

                    }

                }

            }

            else if (PieceColor == Color.Green)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (this.board.Squares[80 + i].Pieces.Count == 0)
                    {
                        return i + 80;

                    }

                }

            }

            return -1;
        }
        public int CalculateTargetSquareIndex(Color PieceColor, int Position)
        {
            if (PieceColor == Color.Yellow)
            {
                if ((Position == 76 || Position == 77 || Position == 78 || Position == 79)&& lastRollWasSix)
                {
                    return 3;
                }

                if(Position == 76 || Position == 77 || Position == 78 || Position == 79)
                {
                    return -1;
                }

               

                if (Position == 0)
                {
                    return (Die.DieValue + 50);

                }

                else if(Position==1)
                    return (Die.DieValue + 51);

                else if (Position >= 52)
                {
                    if (Position + Die.DieValue - 51 == 1)
                    {
                        return 0;
                    }
                    else if (Position + Die.DieValue - 51 == 2)
                    {
                        return 1;
                    }


                    else if (Position + Die.DieValue > 57)
                    {
                        return -1;
                    }

                    else if (Position < 52)
                    {
                        return Position + Die.DieValue - 2;
                    }
                    else return Position + Die.DieValue;
                }

                return Position + Die.DieValue;
            }
            else if (PieceColor == Color.Blue)
            {
                if ((Position == 84 || Position == 85 || Position == 86 || Position == 87) && lastRollWasSix)
                {
                    return 42;
                }
                else if (Position == 84 || Position == 85 || Position == 86 || Position == 87)
                {
                    return -1;
                }

                else if (Position + Die.DieValue > 40 && Position <=40)
                {
                    return (63 + (Position + Die.DieValue - 40));

                }

                else if (Position >= 64)
                {
                    if (Position + Die.DieValue > 69)
                    {
                        return -1;
                    }

                    else return (Position + Die.DieValue);

                }

                else if (Position >= 42 && Position <= 51)
                {
                    if (Position + Die.DieValue > 51)
                    {

                        return (Position + Die.DieValue - 52);
                    }

                }
                return Position + Die.DieValue;
            }

            else if (PieceColor == Color.Green)
            {
                if ((Position == 80 || Position == 81 || Position == 82 || Position == 83) && lastRollWasSix)
                {
                    return 16;
                }

                if (Position == 80 || Position == 81 || Position == 82 || Position == 83)
                {
                    return -1;
                }

                if (Position >= 16 && Position<=51)
                {

                    if (Position + Die.DieValue > 51)
                    {
                        return (Position + Die.DieValue - 52);
                    }
                    else return Position + Die.DieValue;

                }

                else if (Position  >=0 && Position <=14)
                {
                    if (Position + Die.DieValue <= 14)
                    {
                        return (Position + Die.DieValue);
                    }
                    else if(Position+Die.DieValue>=21)
                    {
                        return -1;
                        
                    }

                    else return ((Position + Die.DieValue - 14) + 57);
                }

                else if (Position >= 58 && Position <= 63)
                {
                    if (Position + Die.DieValue <= 63)
                    {
                        return Position + Die.DieValue;

                    }

                    else
                    {
                        return -1;

                    }

                }

                
                

            }

            else if (PieceColor == Color.Red)
            {
                if ((Position == 88 || Position == 89 || Position == 90 || Position == 91) && lastRollWasSix)
                {
                    return 29;
                }

                if (Position == 88 || Position == 89 || Position == 90 || Position == 91)
                {
                    return -1;
                }

                if (Position >= 29 && Position <= 51)
                {
                    if (Position + Die.DieValue <= 51)
                    {
                        return Position + Die.DieValue;

                    }

                    else return (Position + Die.DieValue - 52);

                }

                if (Position >= 0 && Position <= 27)
                {
                    if (Position + Die.DieValue <= 27)
                    {
                        return Position + Die.DieValue;

                    }

                    else if(Position+Die.DieValue>27)
                    {
                        return (Position + Die.DieValue - 28) + 70;
                    }

                }

                if (Position >= 70 && Position <= 74)
                {
                    if (Position + Die.DieValue <= 75)
                    {
                        return Position + Die.DieValue;

                    }

                    else
                    {
                        return -1;

                    }

                }

             

            }


            return -1;
        }

        public Boolean canMovePiece(Player player,Die die)
        {
            if (die.DieValue != 6 && player.GoatsOut == 0)
            {
                return false;
            }

            if (die.DieValue == 6 && player.GoatsOut < 4)
            {
                return true;
            }

            if (player.Color == Color.Red)
            {

                int goatsingoalpath = 0;

                for (int i = 70; i < 76; i++)
                {
                    
                    if(this.board.Squares[i].Pieces.Count>0)
                    {
                        goatsingoalpath = goatsingoalpath + this.board.Squares[i].Pieces.Count;

                        if (i + die.DieValue <= 75)
                        {
                            return true;

                        }

                    }

                }

                if (player.GoatsOut - goatsingoalpath > 0)
                {
                    return true;
                }


            }
            else if (player.Color == Color.Green)
            {
                int goatsingoalpath = 0;

                for (int i = 58; i <=63; i++)
                {
                    
                    if (this.board.Squares[i].Pieces.Count > 0)
                    {
                        goatsingoalpath = goatsingoalpath+this.board.Squares[i].Pieces.Count;

                        if (i + die.DieValue <= 63)
                        {
                            return true;

                        }
                        

                    }

                }

                if (player.GoatsOut - goatsingoalpath > 0)
                {
                    return true;
                }



            }

            else if (player.Color == Color.Blue)
            {
                int goatsingoalpath = 0;

                for (int i = 64; i <=69; i++)
                {
                    
                    if (this.board.Squares[i].Pieces.Count > 0)
                    {
                        goatsingoalpath = goatsingoalpath + this.board.Squares[i].Pieces.Count;

                        if (i + die.DieValue <= 69)
                        {
                            return true;

                        }

                    }

                }

                if (player.GoatsOut - goatsingoalpath > 0)
                {
                    return true;
                }



            }

            else if (player.Color == Color.Yellow)
            {
                int goatsingoalpath = 0;

                for (int i = 52; i <= 57; i++)
                {
                    
                    if (this.board.Squares[i].Pieces.Count > 0)
                    {
                        goatsingoalpath = goatsingoalpath + this.board.Squares[i].Pieces.Count;

                        if (i + die.DieValue <= 57)
                        {
                            return true;

                        }

                    }

                }

                if (player.GoatsOut - goatsingoalpath > 0)
                {
                    return true;
                }

            }




            return false;
        }
        public void RollDie()
        {
            if (nextAction == Action.RollDie)
            {
                if (Die.RollDie() == 6)
                {
                    lastRollWasSix = true;


                    if (canMovePiece(playersTurn, Die))
                    {
                        nextAction = Action.MovePiece;
                    }
                    else
                    {

                        NextPlayersTurn();

                    }
                }
                else
                {
                    lastRollWasSix = false;

                    if (canMovePiece(playersTurn, Die))
                    {
                        nextAction = Action.MovePiece;
                    }
                    else
                    {

                        NextPlayersTurn();

                    }
                    

                }

            }

            if (areAllFinished())
            {
                nextAction = Action.Nothing;
            }


            foreach (Player player in this.Players)
            {
                UpdatePlayer(player);
            }

            NotifyObservers();
            
        }

       
        private void NextPlayersTurn()
        {
            if (playersturn < this.players.Count - 1)
            {
                playersturn++;
                playersTurn = this.players[playersturn];
            }
            else if (playersturn == this.players.Count - 1)
            {
                playersturn=0;
                playersTurn = this.players[playersturn];
            }

            if (playersTurn.HasWon)
            {
                NextPlayersTurn();

            }
           
        }

    }
}
*/