﻿using UnityEngine;
using System.Collections;

public class YellowToken2 : MonoBehaviour {

    public static YellowToken2 Instance;
    int yellow1Index;
    static Animator animator;
    // Use this for initialization
    public Animation anim;
    void Start()
    {
        animator = GetComponent<Animator>();
        yellow1Index = 0;
        ////
        /* anim = GetComponent<Animation>();
         AnimationCurve curve = AnimationCurve.Linear(0, 1, 2, 3);
         AnimationClip clip = new AnimationClip();
         clip.legacy = true;
         clip.SetCurve("", typeof(Transform), "localPosition.x", curve);
         anim.AddClip(clip, "test");
         anim.Play("test");*/
        Instance = this;
    }

    // Update is called once per frame

    public void player1Start()
    {
        //if(Input.GetMouseButtonDown(0))

        {
            print("Player 1 Turn");
            //Condition for first start by 6
            if (DiceSwipeControl.Instance.diceCount == 6)
            {
                TokenMove.Instance.board[0] = (int)TokenNo.tokens.yellowToken;
                animator.SetInteger("AnimState", 1);


            }
            else
            {
                animator.SetInteger("AnimState", 0);
                //Dice.playerTurn++;
            }

        }

    }
    public void player1Turn()
    {

        if (TokenMove.Instance.board[yellow1Index] == 1)
        {
            yellow1Index = +DiceSwipeControl.Instance.diceCount;
            TokenMove.Instance.board[yellow1Index] = 1;
            // for (int i = 1; i <= DiceSwipeControl.Instance.diceCount; i++)
            {
                //animator.Play("YellowFirstMove1-2");
                animator.SetInteger("TokenMove", DiceSwipeControl.Instance.diceCount);

            }
        }
    }
  
}
