﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
    public string loadLevel;
    private Animator animator;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseOver()
    {
        // Animator.animation.Play("start");
        animator.SetInteger("AnimState", 1);
    }
    void OnMouseExit()
    {
        animator.SetInteger("AnimState", 0);

    }
    void OnTriggerEnter(Collider target)
    {
        Application.LoadLevel(loadLevel);
    }
}
