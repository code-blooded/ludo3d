﻿using UnityEngine;
using System.Collections;

public class BoardMove : MonoBehaviour {
    //private int
   private Animator animator;
   
    // Use this for initialization
    void Start () {
        
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        
        if (DiceSwipeControl.Instance.diceCount == 1 && DiceSwipeControl.Instance.diceCount > 0)
        {
            animator.SetInteger("AnimState", 1);
        }
        else if (DiceSwipeControl.Instance.diceCount == 2 && DiceSwipeControl.Instance.diceCount > 0)
        {
            animator.SetInteger("AnimState", 1);
        }
        else if (DiceSwipeControl.Instance.diceCount == 3 && DiceSwipeControl.Instance.diceCount > 0)
        {
            animator.SetInteger("AnimState", 1);
        }
        else if (DiceSwipeControl.Instance.diceCount == 4 && DiceSwipeControl.Instance.diceCount > 0)
        {
            animator.SetInteger("AnimState", 1);
        }
        else if (DiceSwipeControl.Instance.diceCount == 5 && DiceSwipeControl.Instance.diceCount > 0)
        {
            animator.SetInteger("AnimState", 1);
        }
        else if (DiceSwipeControl.Instance.diceCount == 6)
        {
            animator.SetInteger("AnimState", 0);
        }
       

    }
}
