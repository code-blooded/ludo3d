﻿using UnityEngine;
using System.Collections;

public class RedToken1 : MonoBehaviour {

    public static RedToken1 Instance;
    int red4Index;
    static Animator animator;
    // Use this for initialization
    public Animation anim;
    void Start()
    {
        animator = GetComponent<Animator>();
        red4Index = 26;
        ////
        /* anim = GetComponent<Animation>();
         AnimationCurve curve = AnimationCurve.Linear(0, 1, 2, 3);
         AnimationClip clip = new AnimationClip();
         clip.legacy = true;
         clip.SetCurve("", typeof(Transform), "localPosition.x", curve);
         anim.AddClip(clip, "test");
         anim.Play("test");*/
        Instance = this;
    }

    // Update is called once per frame

    public void player3Start()
    {
        //if(Input.GetMouseButtonDown(0))

        {
            print("Player 1 Turn");
            //Condition for first start by 6
            if (DiceSwipeControl.Instance.diceCount == 6)
            {
                TokenMove.Instance.board[26] = (int)TokenNo.tokens.redToken;
                animator.SetInteger("AnimState", 1);


            }
            else
            {
                animator.SetInteger("AnimState", 0);
                //Dice.playerTurn++;
            }

        }

    }
    public void player3Turn()
    {

        if (TokenMove.Instance.board[red4Index] == 3)
        {
            red4Index = +DiceSwipeControl.Instance.diceCount;
            TokenMove.Instance.board[red4Index] = (int)TokenNo.tokens.redToken;
            // for (int i = 1; i <= DiceSwipeControl.Instance.diceCount; i++)
            {
                //animator.Play("YellowFirstMove1-2");
                animator.SetInteger("TokenMove", DiceSwipeControl.Instance.diceCount);

            }
        }
        if (red4Index > 52)
        {
            red4Index = 0;
        }
    }
}
