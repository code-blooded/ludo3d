﻿using UnityEngine;
using System.Collections;

public class GreenToken3 : MonoBehaviour {

    public static GreenToken3 Instance;
    int green4Index;
    static Animator animator;
    // Use this for initialization
    public Animation anim;
    void Start()
    {
        animator = GetComponent<Animator>();
        green4Index = 13;
        ////
        /* anim = GetComponent<Animation>();
         AnimationCurve curve = AnimationCurve.Linear(0, 1, 2, 3);
         AnimationClip clip = new AnimationClip();
         clip.legacy = true;
         clip.SetCurve("", typeof(Transform), "localPosition.x", curve);
         anim.AddClip(clip, "test");
         anim.Play("test");*/
        Instance = this;
    }

    // Update is called once per frame

    public void player2Start()
    {
        //if(Input.GetMouseButtonDown(0))

        {
            print("Player 1 Turn");
            //Condition for first start by 6
            if (DiceSwipeControl.Instance.diceCount == 6)
            {
                TokenMove.Instance.board[13] = (int)TokenNo.tokens.greenToken;
                animator.SetInteger("AnimState", 1);


            }
            else
            {
                animator.SetInteger("AnimState", 0);
                //Dice.playerTurn++;
            }

        }

    }
    public void player2Turn()
    {

        if (TokenMove.Instance.board[green4Index] == 2)
        {
            green4Index = +DiceSwipeControl.Instance.diceCount;
            TokenMove.Instance.board[green4Index] = (int)TokenNo.tokens.greenToken;
            // for (int i = 1; i <= DiceSwipeControl.Instance.diceCount; i++)
            {
                //animator.Play("YellowFirstMove1-2");
                animator.SetInteger("TokenMove", DiceSwipeControl.Instance.diceCount);

            }
        }
        if (green4Index > 52)
        {
            green4Index = 0;
        }
    }
}
